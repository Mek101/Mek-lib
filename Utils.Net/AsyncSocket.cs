﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;


namespace Mek_lib.Utils.Net
{
	using TupleTimerContainer = Tuple<TaskCompletionSource<SocketError>, Timer>;
	using TupleNotifierContainer = Tuple<Action<IAsyncResult>, TaskCompletionSource<SocketError>>;


	/// <summary>
	/// Asyncronous wrapper for the <see cref="Socket"/> class.
	/// </summary>
    class AsyncSocket : IDisposable
	{
		#region Fields and proprieties
		public Socket BaseSocket { get; private set; }
		public EndPoint LocalHost => BaseSocket.LocalEndPoint;
		public EndPoint RemoteHost => BaseSocket.RemoteEndPoint;
		public bool IsConnected => BaseSocket.Connected;
		public SocketError Status
		{
			get { lock(_statusLock) return _status; }
			private set
			{
				lock(_statusLock)
					if(_status != value)
					{
						_status = value;

						if(OnStatusChanged != null)
							OnStatusChanged.BeginInvoke(this, value, (ar) => OnStatusChanged.EndInvoke(ar), null);
					}
			}
		}

		private SocketError _status;

		// Intervals for the timeouts.
		public double ConnectTimeoutInterval;
		public double DisconnectTimeoutInterval;
		public double ReceiveTimeoutInterval;
		public double SendTimeoutInterval;

		// Size of the internal buffer.
		protected readonly int _bufferSize;

		protected bool _autoReceive;

		// Provides atomic operations for the respective fields.
		protected object _receiveLock;
		protected object _statusLock;

		// The handler form the last receiving operation called
		protected Task<SocketError> _lastReceiveHandler;
		// Thread for auto receiving.
		protected System.Threading.Thread _listener;

		protected bool _disposed;

		/// <summary>
		/// Occurs when new data has been received.
		/// </summary>
		public event EventHandler<byte[]> OnDataReceived;

		/// <summary>
		/// Occurs when the status changes.
		/// </summary>
	    public event EventHandler<SocketError> OnStatusChanged;
		#endregion


		#region Construct
		public AsyncSocket(EndPoint localHost, SocketType socketType, ProtocolType protocolType, int bufferSize = 500)
			: this(localHost, socketType, protocolType, -1, -1, -1, bufferSize) { }

		public AsyncSocket(EndPoint localHost, SocketType socketType, ProtocolType protocolType, double connectTimeout, double disconnectTimeout, double receiveTimeout, double sendTimeout, int bufferSize = 500)
		{
			BaseSocket = new Socket(socketType, protocolType);
			BaseSocket.Bind(localHost);
			// The sockets blocks the caller when syncronously polled
			BaseSocket.Blocking = true;

			Status = SocketError.NotConnected;

			ConnectTimeoutInterval = connectTimeout;
			DisconnectTimeoutInterval = disconnectTimeout;
			ReceiveTimeoutInterval = receiveTimeout;
			SendTimeoutInterval = sendTimeout;

			_bufferSize = bufferSize;

			// Automatic listening is disabled by default
			_autoReceive = false;

			// Initializes locks
			_receiveLock = new object();
			_statusLock = new object();

			_disposed = false;
		}

		~AsyncSocket() { Dispose(); }

		public void Dispose()
		{
			if(!_disposed)
			{
				Status = SocketError.NoRecovery;

				if(_lastReceiveHandler != null)
				{
					_lastReceiveHandler.Dispose();
					_lastReceiveHandler = null;
				}

				if(BaseSocket != null)
				{
					if(BaseSocket.Connected)
					{
						BaseSocket.Blocking = false;
						StopReceving(true);
						BaseSocket.Shutdown(SocketShutdown.Both);
						BaseSocket.Close();
					}
					
					BaseSocket = null;
				}
				
				_receiveLock = null;
				_statusLock = null;

				_disposed = true;
			}
		}
		#endregion


		#region Utils Methods
		/// <summary>
		/// Returns whatever the actual status is SocketError.IsConnected or
		/// SocketError.Success and it's safe to interact with the socket.
		/// </summary>
		protected bool IsSafeStatus
		{
			get
			{
				if(BaseSocket.Connected)
					lock(_statusLock)
						return _status == SocketError.IsConnected || _status == SocketError.Success;
				else
					return false;
			}
		}


		/// <summary>
		/// Terminates an operation that throw an exception, returning a task with the exception set, setting the error code in Status if the exception is a <see cref="SocketException"/>
		/// </summary>
		/// <returns>The exception task.</returns>
		/// <param name="e">E.</param>
		protected Task<SocketError> GetExceptionTask(Exception e)
		{
			if(e is SocketException)
				Status = ((SocketException)e).SocketErrorCode;

			return Task.FromException<SocketError>(e);
		}


		/// <summary>
		/// Terminates an operation that throw an exception, setting the excption in the <paramref name="notifier"/> and the error code in Status if the exception is a <see cref="SocketException"/>
		/// </summary>
		/// <returns>The exception task.</returns>
		/// <param name="e">E.</param>
		protected bool SetExceptionTask(Exception e, TaskCompletionSource<SocketError> notifier)
		{
			if(e is SocketException)
				Status = ((SocketException)e).SocketErrorCode;

			return notifier.TrySetException(e);
		}


        /// <summary>
        /// Stops and disposes of the <seealso cref="Timer"/> instance.
        /// </summary>
        /// <param name="timer">The timer to terminate.</param>
        protected void TerminateTimer(Timer timer)
        {
            timer.Stop();
			timer.Dispose();
        }


		/// <summary>
		/// Terminates an operation that throw an exception, returning a task with the exception set, setting the error code in Status if the exception is a <see cref="SocketException"/> and disposing the <paramref name="timeout"/> if not null.
		/// </summary>
		/// <returns>The task exception.</returns>
		/// <param name="e">Exception to put in the task.</param>
		protected Task<SocketError> TermianteOperation(Exception e, Timer timeout)
		{
			if(timeout != null)
                TerminateTimer(timeout);

			return GetExceptionTask(e);
		}


        /// <summary>
        /// Attemps to set the <seealso cref="TimeoutException"/> in the given <paramref name="notifier"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="notifier"></param>
        /// <param name="message"></param>
        protected void TimeoutOperation(Timer sender, TaskCompletionSource<SocketError> notifier, string message)
        {
            notifier.TrySetException(new TimeoutException(message));
            TerminateTimer(sender);
        }


		/// <summary>
		/// <c>True</c> if the socket is listening for data, <c>false</c> otherwise.
		/// </summary>
		public bool AutoReceive
		{
			get { lock(_receiveLock) return _autoReceive; }
			private set { lock(_receiveLock) _autoReceive = value; }
		}
		#endregion


		#region Connect Methods
		/// <summary>
		/// Asyncronously connects to the given remote endpoint, providing a task for waiting the operation.
		/// </summary>
		/// <param name="remoteHost"></param>
		/// <returns><c>true</c> if the operation begin, <c>false</c> otherwise.</returns>
		public Task<SocketError> ConnectAsync(EndPoint remoteHost)
		{
			TaskCompletionSource<SocketError> notifier = new TaskCompletionSource<SocketError>();
			Timer timeout = null;
			AsyncCallback callback = null;
			object container = null;

			try
			{
				// If the timeouts are enabled.
				if(ConnectTimeoutInterval <= 0)
				{
					callback = ConnectCallback;
					container = notifier;
				}
				else
				{
					timeout = new Timer(ConnectTimeoutInterval);
					timeout.AutoReset = false;
					timeout.Elapsed += (s, e) => TimeoutOperation(timeout, notifier, "Set timeout for connecting has expired.");

					callback = ConnectTimedCallback;
					container = new TupleTimerContainer(notifier, timeout);
					timeout.Start();
				}

				BaseSocket.BeginConnect(remoteHost, callback, container);
				return notifier.Task;
			}
			catch(Exception e) { return TermianteOperation(e, timeout); }
		}


		/// <summary>
		/// Terminates the asyncronous connection with a remote host and sets the handler result.
		/// </summary>
		/// <param name="asyncResult"></param>
		protected void ConnectCallback(IAsyncResult asyncResult)
		{
			TaskCompletionSource<SocketError> notifier = (TaskCompletionSource<SocketError>)asyncResult.AsyncState;
			EndConnection(asyncResult, notifier);
		}


		/// <summary>
		/// Terminates the asyncronous connection with a remote host after stopping the timer and sets the handler result.
		/// </summary>
		/// <param name="asyncResult"></param>
		protected void ConnectTimedCallback(IAsyncResult asyncResult)
		{
			TupleTimerContainer tuple = (TupleTimerContainer)asyncResult.AsyncState;
			TaskCompletionSource<SocketError> notifier = tuple.Item1;
			Timer timeout = tuple.Item2;

            TerminateTimer(timeout);

            EndConnection(asyncResult, notifier);
		}


		protected void EndConnection(IAsyncResult asyncResult, TaskCompletionSource<SocketError> notifier)
		{
			try
			{
				BaseSocket.EndConnect(asyncResult);

				Status = SocketError.IsConnected;
				notifier.TrySetResult(Status);
			}
			catch(Exception e) { SetExceptionTask(e, notifier); }
		}
		#endregion


		#region Accept Methods
		/// <summary>
		/// Asyncronously begins to accept a remote connection, providing a task for waiting the operation and provviding a timeout if enabled.
		/// </summary>
		/// <returns>The handler task.</returns>
		/// <param name="listenBacklog">Backlog for putting a sockett in listeing state.</param>
		public Task<SocketError> AcceptAsync(int listenBacklog)
		{
			TaskCompletionSource<SocketError> notifier = new TaskCompletionSource<SocketError>();
			Timer timeout = null;
			AsyncCallback callback = null;
			object container = null;

			try
			{
				BaseSocket.Listen(listenBacklog);

				if(ConnectTimeoutInterval <= 0)
				{
					callback = AcceptCallback;
					container = notifier;
				}
				else
				{
					timeout = new Timer(ConnectTimeoutInterval);
					timeout.AutoReset = false;
                    timeout.Elapsed += (s, e) => TimeoutOperation(timeout, notifier, "Set timeout for accepting has expired.");

					callback = AcceptTimedCallback;
					container = new TupleTimerContainer(notifier, timeout);
					timeout.Start();
				}

				BaseSocket.BeginAccept(callback, container);
				return notifier.Task;
			}
			catch(Exception e) { return TermianteOperation(e, timeout); }
		}


		/// <summary>
		/// Callback for asyncronous operation of accepting a remote connection, sets the handler result.
		/// </summary>
		/// <param name="asyncResult"></param>
		protected void AcceptCallback(IAsyncResult asyncResult)
		{
			TaskCompletionSource<SocketError> notifier = (TaskCompletionSource<SocketError>)asyncResult.AsyncState;
			EndAccept(asyncResult, notifier);
		}


		/// <summary>
		/// Callback for asyncronous operation of accepting a remote connection, stops the timer and sets the handler result.
		/// </summary>
		/// <param name="asyncResult"></param>
		protected void AcceptTimedCallback(IAsyncResult asyncResult)
		{
			TupleTimerContainer tuple = (TupleTimerContainer)asyncResult.AsyncState;
			TaskCompletionSource<SocketError> notifier = tuple.Item1;
			Timer timeout = tuple.Item2;

			TerminateTimer(timeout);

			EndAccept(asyncResult, notifier);
		}


		/// <summary>
		/// Termiantes the asyncronous operation of accepting a remote connection and sets the handler result.
		/// </summary>
		protected void EndAccept(IAsyncResult asyncResult, TaskCompletionSource<SocketError> notifier)
		{
			try
			{
				// Re-assigns the socket
				Socket temp = BaseSocket.EndAccept(asyncResult);
				BaseSocket.Close();
				BaseSocket = temp;

				Status = SocketError.IsConnected;

				notifier.SetResult(Status);
			}
			catch(Exception e) { SetExceptionTask(e, notifier); }
		}
		#endregion


		#region Disconnect Methods
		/// <summary>
		/// Asyncronously begins to disconnect from the remote host, starting a timeout if enabled and providing a task for waiting the operation.
		/// </summary>
		/// <param name="reuseSocket"></param>
		/// <returns></returns>
		public Task<SocketError> DisconnectAsync(bool reuseSocket)
		{
			TaskCompletionSource<SocketError> notifier = new TaskCompletionSource<SocketError>();
			Timer timeout = null;
			AsyncCallback callback = null;
			object container = null;

			try
			{
				if(DisconnectTimeoutInterval <= 0)
				{
					callback = DisconnectCallback;
					container = notifier;
				}
				else
				{
					timeout = new Timer(DisconnectTimeoutInterval);
					timeout.AutoReset = false;
					timeout.Elapsed += (s, e) => TimeoutOperation(timeout, notifier, "Set timeout for disconnecting has expired.");

					callback = DisconnectTimedCallback;
					container = new TupleTimerContainer(notifier, timeout);
					timeout.Start();
				}

				BaseSocket.BeginDisconnect(reuseSocket, callback, container);
				return notifier.Task;
			}
			catch(Exception e) { return TermianteOperation(e, timeout); }
		}


		/// <summary>
		/// Callback for the asyncronous operation of disconnecting from a remote host and sets the handler result.
		/// </summary>
		/// <param name="asyncResult"></param>
		protected void DisconnectCallback(IAsyncResult asyncResult)
		{
			TaskCompletionSource<SocketError> notifier = (TaskCompletionSource<SocketError>)asyncResult.AsyncState;
			EndDisconnect(asyncResult, notifier);
		}


		protected void DisconnectTimedCallback(IAsyncResult asyncResult)
		{
			TupleTimerContainer tuple = (TupleTimerContainer)asyncResult.AsyncState;
			TaskCompletionSource<SocketError> notifier = tuple.Item1;
			Timer timeout = tuple.Item2;

            TerminateTimer(timeout);

			EndDisconnect(asyncResult, notifier);
		}


		/// <summary>
		/// Terminates the asyncronous operation of disconnecting from a remote host and sets the handler result.
		/// </summary>
		protected void EndDisconnect(IAsyncResult asyncResult, TaskCompletionSource<SocketError> notifier)
		{
			try
			{
				BaseSocket.EndDisconnect(asyncResult);
				Status = SocketError.NotConnected;
				notifier.TrySetResult(Status);
			}
			catch (Exception e) { SetExceptionTask(e, notifier); }
		}
        #endregion


		#region Closing and shutdown
		/// <summary>
		/// Disable sends and/or receives on a socket.
		/// </summary>
		/// <param name="how">Specifies the operation no longer being allowed.</param>
		/// <returns></returns>
		public Task<SocketError> ShutdownAsync(SocketShutdown how)
		{
			try
			{
				BaseSocket.Shutdown(how);
				return Task.FromResult<SocketError>(SocketError.Shutdown);
			}
			catch(Exception e) { return GetExceptionTask(e); }
		}


		/// <summary>
		/// Asyncronously begins to close the socket, providing a task for waiting the operation.
		/// </summary>
		/// <returns>The async.</returns>
		public Task<SocketError> CloseAsync()
		{
			TaskCompletionSource<SocketError> notifier = new TaskCompletionSource<SocketError>();

			try
			{
				if(DisconnectTimeoutInterval < 0)
				{
					// Without timer.
					Action close = BaseSocket.Close;
					Action<IAsyncResult> end = close.EndInvoke;

					close.BeginInvoke(CloseCallback, new TupleNotifierContainer(end, notifier));
				}
				else
				{
					// With timer.
					Action<int> close = BaseSocket.Close;
					Action<IAsyncResult> end = close.EndInvoke;

					close.BeginInvoke((int)DisconnectTimeoutInterval, CloseCallback, new TupleNotifierContainer(end, notifier));
				}

				return notifier.Task;
			}
			catch(Exception e) { return GetExceptionTask(e); }
		}


		/// <summary>
		/// Terminates the asyncronous closing process and sets the handler result.
		/// </summary>
		/// <param name="asyncResult"></param>
		protected void CloseCallback(IAsyncResult asyncResult)
		{
			TupleNotifierContainer tuple = (TupleNotifierContainer)asyncResult.AsyncState;
			Action<IAsyncResult> end = tuple.Item1;
			TaskCompletionSource<SocketError> notifier = tuple.Item2;

			try
			{
				end.EndInvoke(asyncResult);
				notifier.TrySetResult(SocketError.NotConnected);
			}
			catch(Exception e) { SetExceptionTask(e, notifier); }
		}
		#endregion


		#region Receive Methods
		/// <summary>
		/// Asyncronously begins to receive data, providing a task for waiting the operation.
		/// </summary>
		/// <returns></returns>
		public Task<SocketError> ReceiveAsync(SocketFlags flags)
		{
			TaskCompletionSource<SocketError> notifier = new TaskCompletionSource<SocketError>();
			Timer timeout = null;
			byte[] buffer = new byte[_bufferSize];
			AsyncCallback callback = null;
			object container = null;

			try
			{
				if(ReceiveTimeoutInterval <= 0)
				{
					callback = ReceiveCallback;
					container = new Tuple<byte[], TaskCompletionSource<SocketError>>(buffer, notifier);
				}
				else
				{
					timeout = new Timer(ReceiveTimeoutInterval);
					timeout.AutoReset = false;
                    timeout.Elapsed += (s, e) => TimeoutOperation(timeout, notifier, "Set timeout for receiving has expired.");

					callback = ReceiveTimedCallback;
					container = new Tuple<byte[], TaskCompletionSource<SocketError>, Timer>(buffer, notifier, timeout);
					timeout.Start();
				}

				BaseSocket.BeginReceive(buffer, 0, _bufferSize, flags, callback, container);
				// Tracks the operation
				_lastReceiveHandler = notifier.Task;
				return notifier.Task;
			}
			catch(Exception e) { return TermianteOperation(e, timeout); }
		}


		/// <summary>
		/// Terminates the asyncronous receiving process, recursivly calling it if the socket is in a safe state and sets the handler result.
		/// </summary>
		/// <param name="asyncResult"></param>
		protected void ReceiveCallback(IAsyncResult asyncResult)
		{
			Tuple<byte[], TaskCompletionSource<SocketError>> tuple = (Tuple<byte[], TaskCompletionSource<SocketError>>)asyncResult.AsyncState;
			byte[] buffer = tuple.Item1;
			TaskCompletionSource<SocketError> notifier = tuple.Item2;

			EndReceive(asyncResult, buffer, notifier);
		}


		protected void ReceiveTimedCallback(IAsyncResult asyncResult)
		{
			Tuple<byte[], TaskCompletionSource<SocketError>, Timer> tuple = (Tuple<byte[], TaskCompletionSource<SocketError>, Timer>)asyncResult.AsyncState;
			byte[] buffer = tuple.Item1;
			TaskCompletionSource<SocketError> notifier = tuple.Item2;
			Timer timeout = tuple.Item3;

            TerminateTimer(timeout);

			EndReceive(asyncResult, buffer, notifier);
		}


		protected void EndReceive(IAsyncResult asyncResult, byte[] buffer, TaskCompletionSource<SocketError> notifier)
		{
			try
			{
				int read = ReadReceived(asyncResult);
				byte[] data = ExtractBuffer(buffer, read);

				// Raises the event asyncronously
				if(OnDataReceived != null)
					OnDataReceived.BeginInvoke(this, data, (ar) => OnDataReceived.EndInvoke(ar), null);

				notifier.SetResult(Status);
			}
			catch(Exception e) { SetExceptionTask(e, notifier); }
		}


		/// <summary>
		/// Terminates the async call.
		/// </summary>
		/// <param name="asyncResult"></param>
		/// <returns>The size of the buffer with data.</returns>
		protected int ReadReceived(IAsyncResult asyncResult)
		{
			SocketError status;
			int read = BaseSocket.EndReceive(asyncResult, out status);
			Status = status;

			return read;
		}


		/// <summary>
		/// Extracts the data from the <paramref name="buffer"/>.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="count"></param>
		protected byte[] ExtractBuffer(byte[] buffer, int count)
		{
			IEnumerable<byte> segment = buffer.Take(count);
			return segment.ToArray();
		}
		#endregion


		#region Send Methods
		/// <summary>
		/// Asyncronously sends a string of text to the remote host, providing a task for waiting the operation.
		/// </summary>
		/// <param name="str">String to send.</param>
		/// <returns></returns>
		public Task<SocketError> SendAsync(string str, SocketFlags flags) { return SendAsync(Encoding.UTF8.GetBytes(str), flags); }


		/// <summary>
		/// Asyncronously sends a buffer of data of text to the remote host, providing a task for waiting the operation.
		/// </summary>
		/// <param name="data">Buffer to send.</param>
		/// <returns></returns>
		public Task<SocketError> SendAsync(byte[] data, SocketFlags flags) { return SendAsync(data, 0, data.Length, flags); }


		/// <summary>
		/// Asyncronously sends a portion of buffer of data of text to the remote host, providing a task for waiting the operation.
		/// </summary>
		/// <param name="data">Buffer to send.</param>
		/// <param name="offset">Offset to start reading the buffer from.</param>
		/// <param name="count">The count of bytes to send from the offset.</param>
		/// <returns></returns>
		public Task<SocketError> SendAsync(byte[] data, int offset, int count, SocketFlags flags)
		{
			TaskCompletionSource<SocketError> notifier = new TaskCompletionSource<SocketError>();
			Timer timeout = null;
			AsyncCallback callback = null;
			object container = null;

			try
			{
				if(SendTimeoutInterval <= 0)
				{
					callback = SendCallback;
					container = notifier;
				}
				else
				{
					timeout = new Timer(SendTimeoutInterval);
					timeout.AutoReset = false;
                    timeout.Elapsed += (s, e) => TimeoutOperation(timeout, notifier, "Set timeout for sending has expired.");

					callback = SendTimedCallback;
					container = new TupleTimerContainer(notifier, timeout);
					timeout.Start();
				}

				BaseSocket.BeginSend(data, offset, count, flags, callback, container);
				return notifier.Task;
			}
			catch(Exception e) { return TermianteOperation(e, timeout); }
		}


		/// <summary>
		/// Terminates the asyncronous sending process and sets the handler result..
		/// </summary>
		/// <param name="asyncResult"></param>
		protected void SendCallback(IAsyncResult asyncResult)
		{
			TaskCompletionSource<SocketError> notifier = (TaskCompletionSource<SocketError>)asyncResult.AsyncState;
			EndSend(asyncResult, notifier);
		}


		protected void SendTimedCallback(IAsyncResult asyncResult)
		{
			TupleTimerContainer tuple = (TupleTimerContainer)asyncResult.AsyncState;
			TaskCompletionSource<SocketError> notifier = tuple.Item1;
			Timer timeout = tuple.Item2;

            TerminateTimer(timeout);

			EndSend(asyncResult, notifier);
		}


		protected void EndSend(IAsyncResult asyncResult, TaskCompletionSource<SocketError> notifier)
		{
			try
			{
				BaseSocket.EndSend(asyncResult, out SocketError status);
				Status = status;

				notifier.TrySetResult(Status);
			}
			catch(Exception e) { SetExceptionTask(e, notifier); }
		}
        #endregion


        #region Auto receiving
        /// <summary>
        /// The socket begins to listen for data, automatically raising the event.
        /// </summary>
        /// <param name="raiseAsync">Raises the data received event asyncronously if set to <c>true</c>, syncronously if set to <c>false</c>.</param>
        /// <returns><c>true</c> if the automatic receiving has been enabled, <c>false</c> otherwise.</returns>
        public bool StartReceiving(bool raiseAsync)
        {
			if(IsSafeStatus && !AutoReceive)
			{
				// Ends and eventual previous thread.
				StopReceving(false);

				AutoReceive = true;

				 _listener = new System.Threading.Thread(ThreadMain);

				// Lowers priority
				_listener.Priority = System.Threading.ThreadPriority.BelowNormal;
				// Start the listening thread
				_listener.Start(raiseAsync);

				return true;
			}
			else
				return false;
		}


		/// <summary>
		/// Stops listeing for data.
		/// </summary>
		public void StopReceving(bool force)
		{
			AutoReceive = false;

			if(force && _listener != null && _listener.IsAlive)
				_listener.Abort();
		}


		/// <summary>
		/// Listening thread code.
		/// </summary>
		/// <param name="container">Container.</param>
		protected void ThreadMain(object container)
		{
			try
			{
				bool raiseAsync = (bool)container;

				// Waits for eventual receive calls to finish
				if(_lastReceiveHandler != null)
					_lastReceiveHandler.Wait();

				byte[] buffer = new byte[_bufferSize];

				while(AutoReceive && IsSafeStatus)
				{
					try
					{
						// Should block the thread
						int read = BaseSocket.Receive(buffer);

						byte[] data = ExtractBuffer(buffer, read);

						// Raises the event
						if(OnDataReceived != null)
						{
							if(raiseAsync)
								OnDataReceived.BeginInvoke(this, data, (ar) => OnDataReceived.EndInvoke(ar), null);
							else
								OnDataReceived.Invoke(this, data);
						}

					}
					catch(SocketException se)
					{
						AutoReceive = false;
						Status = se.SocketErrorCode;
					}
				}
			}
			// Exists the thread when _listener.Abort() is called
			catch(System.Threading.ThreadAbortException) { return; }
		}
        #endregion
    }
}
