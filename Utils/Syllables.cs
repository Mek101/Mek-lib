﻿using System;
using System.Collections.Generic;

/*
   This file is part of the Mek-lib

   Mek-lib is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Mek-lib is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the Mek-lib.If not, see<http://www.gnu.org/licenses/>.
*/

namespace Mek_lib.Utils
{
	/// <summary>
	/// Scomposes a string into it's syllables or composes it given the syllables
	/// </summary>
	public static class Syllables
	{
		#region Private Methods
		/// <summary>
		/// Tells if the given charter is a vocal
		/// </summary>
		/// <param name="letter">The char to analyze</param>
		/// <returns>True if the charater is a vocal, false otherwise</returns>
		private static bool IsVocal(char letter)
		{
			letter = char.ToLower(letter);

			return letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u';
		}


		/// <summary>
		/// Tells if the two near consonats should be split
		/// </summary>
		/// <param name="first">The first consonant</param>
		/// <param name="second">The second consonant</param>
		/// <returns>True if the charaters should be split, false otherwise</returns>
		private static bool SplitConsonats(char first, char second)
		{
			// Lowers the given charaters
			first = char.ToLower(first);
			second = char.ToLower(second);

			// If there's a vocal or a special case
			return !(IsVocal(first) || IsVocal(second)
					|| first == 'g' && (second == 'l' || second == 'n' || second == 'r')
					|| first == 's' && second == 'c'
					|| first == 'c' && second == 'h'
					|| first == 't' && second == 's');
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Scomposes a string in it's syllables
		/// </summary>
		/// <param name="word">The string i want to scopose</param>
		/// <returns>A string array containig the syllables of the original word</returns>
		public static string[] Scompose(string word)
		{
			// Dynamic sequence 
			List<string> syllables = new List<string>();
			// Represents a syllabae
			string syllable = String.Empty;

			for(int i = 0; i < word.Length; i++)
			{
				// Adds the charater to the current syllable
				syllable += word[i];

				// Splitting is triggered at every vocal, unless it's the first letter or when there's only a charater left
				if(IsVocal(word[i]) || i == word.Length - 1)
				{
					// Splits double consonants 
					if(i + 2 < word.Length && SplitConsonats(word[i + 1], word[i + 2]))
						syllable += word[++i];

					// Ensures double vocals and up in the same syllable
					if(i + 1 < word.Length && IsVocal(word[i]) && IsVocal(word[i + 1]))
					{
						syllable += word[++i];

						// Checks again to split double consonants 
						if(i + 2 < word.Length && SplitConsonats(word[i + 1], word[i + 2]))
							syllable += word[++i];
					}

					/*
                     * If it's still the first letter and there weren't 
                     * any double vocal or consonats, adds the syllable
                     * and resets it for next cycle
                     */
					if(i != 0)
					{
						// Adds the syllable to the sequence
						syllables.Add(syllable);
						// Resets the syllable for the new cycle
						syllable = string.Empty;
					}
				}
			}

			return syllables.ToArray();
		}


		public static string Compose(IEnumerable<string> syllables) { return string.Concat(syllables); }
		#endregion

	}
}
