﻿/*
	This file is part of the Mek-lib

	Mek-lib is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Mek-lib is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with the Mek-lib.If not, see<http://www.gnu.org/licenses/>.
*/

namespace Mek_lib.Utils
{
	/// <summary>
	/// Implements a simple logging system utility.
	/// </summary>
	public class Logger
	{
		#region Fields
		/// <summary>
		/// Logging event handler delegate.
		/// </summary>
		public delegate void LoggingEventHandler(string sender, string log);


		/// <summary>
		/// Occurs when a object calls for a log.
		/// </summary>
		public static event LoggingEventHandler OnLogging;


		/// <summary>
		/// Gets a value indicating whether this <see cref="T:Utils.Logger"/> raises the <seealso cref="OnLogging"/> asyncronously.
		/// </summary>
		/// <value><c>true</c> if it's raised asyncronously; <c>false</c> otherwise.</value>
		public bool RaiseAsync { private set; get; }
		#endregion


		/// <summary>
		/// Initializes a new instance of the <see cref="T:Utils.Logger"/> class.
		/// </summary>
		/// <param name="raiseAsync">If set to <c>true</c> the <seealso cref="OnLogging"/> event will be raised asyncronously.</param>
		public Logger(bool raiseAsync)
		{
			RaiseAsync = raiseAsync;
		}


		/// <summary>
		/// Raises the <seealso cref="OnLogging"/> event.
		/// </summary>
		protected static void RaiseOnLogging(string sender, bool raiseAsync, params object[] logs)
		{
			if(OnLogging != null)
			{
				string text = string.Concat(logs);

				if(raiseAsync)
					OnLogging.BeginInvoke(sender, text, (ar) => OnLogging.EndInvoke(ar), null);
				else
					OnLogging.Invoke(sender, text);
			}
		}


		/// <summary>
		/// Sends a log from the given sender.
		/// </summary>
		/// <param name="sender">The sender object.</param>
		/// <param name="logs">The objects to print.</param>
		public void Log(object sender, params object[] logs)
		{
			RaiseOnLogging(sender.ToString(), RaiseAsync, logs);
		}


		/// <summary>
		/// Sends a log from the given sender.
		/// </summary>
		/// <param name="sender">The sender object.</param>
		/// <param name="logs">The objects to print.</param>
		public void Log(ILoggable sender, params object[] logs)
		{
			RaiseOnLogging(sender.ObjectLogName, RaiseAsync, logs);
		}
	}


	/// <summary>
	/// Implements a dedicated name for logging.
	/// </summary>
	public interface ILoggable
	{
		/// <summary>
		/// Gets the log name of the object.
		/// </summary>
		/// <value>The name of the log object.</value>
		string ObjectLogName { get; }
	}
}
