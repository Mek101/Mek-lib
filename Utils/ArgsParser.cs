﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

/*
   This file is part of the Mek-lib

   Mek-lib is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Mek-lib is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the Mek-lib.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Mek_lib.Utils
{
	/// <summary>
	/// Specifies the response action to an unknow trigger.
	/// </summary>
	public enum OnUnknowTrigger : byte
	{
		/// <summary>Ignores the trigger.</summary>
		None,
		/// <summary>Throws an <see="ArgumentException"/>.</summary>
		Throw,
		/// <summary>Adds the trigger to the unused options.</summary>
		AsUnused
	}


	/// <summary>
	/// Analyzes a sequence of string triggers, raises the associated callbacks.
	/// </summary>
	public class ArgsParser : ICloneable
	{
		#region Internal data types definitions
		// Contains the callback and the associeted metadata.
		protected sealed class CallbackContainer : ICloneable
		{
			public CallbackContainer(Action<string[]> callback, int numOfOptions, char shortTrigger, string longTrigger, bool invokeAsync)
			{
				Callback = callback;
				NumOfOptions = numOfOptions;
				ShortTrigger = shortTrigger;
				LongTrigger = longTrigger;
				InvokeAsync = invokeAsync;
			}

			// The constants for absent triggers.
			public static char NoShortTrigger { get { return '\0'; } }
			public static string NoLongTrigger { get { return "\0"; } }

			// Determinates the constant which specifies to take options until the next trigger.
			public static int Alloptions { get { return -1; } }

			public Action<string[]> Callback { private set; get; }
			public int NumOfOptions { private set; get; }
			public char ShortTrigger { private set; get; }
			public string LongTrigger { private set; get; }
			public bool InvokeAsync { private set; get; }

			public object Clone() { return new CallbackContainer(Callback, NumOfOptions, ShortTrigger, LongTrigger, InvokeAsync); }
		}

		// The existing types of separators
		protected enum ArgumentType : byte { option, ShortTrigger, LongTrigger }
		#endregion


		#region Fields		
		// Holds the callbacks containers.
		protected List<CallbackContainer> _callbacks;

        /// <summary>
        /// Returns <c>true</c> if this <see cref="T:Mek_lib.Utils.ArgsParser"/> treats windows slash arguments separators as such, <c>false</c> if not.
        /// </summary>
        public bool UsingWindowsSlash { protected set; get; }
		#endregion


		/// <summary>
		/// To be filled with callbacks that will be called when the relative trigger argument is found.
		/// </summary>
		/// <param name="firstTriggerOnly">If set to true, only the first trigger found will be called.</param>
		public ArgsParser() : this(IsWindows) { }


		/// <summary>
		/// To be filled with callbacks that will be called when the relative trigger argument is found.
		/// </summary>
		/// <param name="firstTriggerOnly">If set to <c>true</c>, only the first trigger found will be called.</param>
		/// <param name="useWindowsSlash">If set to <c>true</c>, the windows slash will be treated as separator for short arguments.</param>
		public ArgsParser(bool useWindowsSlash) : this(useWindowsSlash, new List<CallbackContainer>()) { }


		/// <summary>
		/// To be filled with callbacks that will be called when the relative trigger argument is found.
		/// Useful to clone the ccurrent object instance.
		/// </summary>
		/// <param name="firstTriggerOnly">If set to <c>true</c>, only the first trigger found will be called.</param>
		/// <param name="useWindowsSlash">If set to <c>true</c>, the windows slash will be treated as separator for short arguments.</param>
		/// <param name="callbacks">The collection containing the callbacks with the assosiciated triggers.</param>
		protected ArgsParser(bool useWindowsSlash, List<CallbackContainer> callbacks)
		{
			UsingWindowsSlash = useWindowsSlash;
			_callbacks = callbacks;
		}


		#region Arguments check
		/// <summary>
		/// Checks the number of options, it must be above zero.
		/// </summary>
		/// <param name="numOfOptions">Number of options.</param>
		protected void CheckNumOfOptions(int numOfOptions)
		{
			if(numOfOptions < 0)
				throw new ArgumentNullException(nameof(numOfOptions), "Cannot use a negative number of options.");
		}


		/// <summary>
		/// Checks the short trigger doesn't contain internal constants charaters.
		/// </summary>
		/// <param name="shortTrigger">Short trigger.</param>
		protected void CheckShortTrigger(char shortTrigger)
		{
			List<Exception> exceptions = new List<Exception>(3);

			if(char.IsWhiteSpace(shortTrigger))
				exceptions.Add(new ArgumentException("Cannot use whitespace character \"" + shortTrigger + "\" as short trigger.", nameof(shortTrigger)));

			if(IsShortSeparator(shortTrigger))
				exceptions.Add(new ArgumentException("Cannot use the argument separator " + shortTrigger + " as short trigger.", nameof(shortTrigger)));

			if(shortTrigger == CallbackContainer.NoShortTrigger)
				exceptions.Add(new ArgumentException("Cannot use the null character as option.", nameof(shortTrigger)));

			if(exceptions.Count == 1)
				throw exceptions[0];
			else if(exceptions.Count > 1)
				throw new AggregateException(exceptions);
			else
				exceptions.Clear();
		}


		/// <summary>
		/// Checks the long trigger doesn't contain internal constants charaters and isn't null or void.
		/// </summary>
		/// <param name="longTrigger">Long trigger.</param>
		protected void CheckLongTrigger(string longTrigger)
		{
			List<Exception> exceptions = new List<Exception>(4);

			if(longTrigger.Contains(' ') || longTrigger.Contains('\t'))
				exceptions.Add(new ArgumentException("Cannot use whiespaces chararters in trigger strings.", nameof(longTrigger)));
			if(string.IsNullOrEmpty(longTrigger) || string.IsNullOrWhiteSpace(longTrigger))
				exceptions.Add(new ArgumentNullException(nameof(longTrigger), "Cannot use null or empty string as a trigger."));

			if(IsShortSeparator(longTrigger[0]))
				exceptions.Add(new ArgumentException("Cannot use the argument separator " + longTrigger[0] + " as first character for the long trigger " + longTrigger + ".", nameof(longTrigger)));

			if(longTrigger == CallbackContainer.NoLongTrigger)
				exceptions.Add(new ArgumentException("Cannot use string containing invalied character \"" + CallbackContainer.NoLongTrigger + "\" as trigger.", nameof(longTrigger)));

			if(exceptions.Count == 1)
				throw exceptions[0];
			else if(exceptions.Count > 1)
				throw new AggregateException(exceptions);
			else
				exceptions.Clear();
		}
		#endregion


		#region Utilities
		/// <summary>
		/// Detects if the current platfomr is Windows.
		/// </summary>
		/// <returns><c>true</c>, if the platform is windows <c>false</c> otherwise.</returns>
		// Wouldn't be able to use it into the constructor if it wasn't static.
		protected static bool IsWindows
		{
			get
			{
				PlatformID platform = Environment.OSVersion.Platform;
				return platform != PlatformID.Unix || platform != PlatformID.MacOSX;
			}
		}


		/// <summary>
		/// Returns if the given char is a short argument separator on the current platform.
		/// </summary>
		/// <param name="c">Char to analyze.</param>
		/// <returns></returns>
		protected bool IsShortSeparator(char c) { return c == '-' || (UsingWindowsSlash && c == '/'); }


		/// <summary>
		/// Detects the type of the given argument.
		/// </summary>
		/// <returns>The argument type.</returns>
		/// <param name="arg">The argument to analyze.</param>
		protected ArgumentType GetArgumentType(string arg)
		{
			if(arg.Length > 1)
			{
				if(UsingWindowsSlash && arg[0] == '/')
				{
					if(arg.Length > 2)
						return ArgumentType.LongTrigger;
					else
						return ArgumentType.ShortTrigger;
				}
				else if(arg[0] == '-')
				{
					if(arg.Length > 2 && arg[1] == '-')
						return ArgumentType.LongTrigger;
					else
						return ArgumentType.ShortTrigger;
				}
			}

			return ArgumentType.option;
		}


		/// <summary>
		/// Removes the separator fro mthe trigger or argument.
		/// </summary>
		/// <returns>The stripped trigger or argument.</returns>
		/// <param name="trigger">Trigger or argument to strip.</param>
		/// <param name="type">The type of trigger or argument.</param>
		protected string StripSeparator(string trigger, ArgumentType type)
		{
			if(type == ArgumentType.LongTrigger)
				return trigger.Remove(0, 2);
			else if(type == ArgumentType.ShortTrigger)
				return trigger.Remove(0, 1);
			else
				return trigger;
		}
		#endregion


		/// <summary>
		/// Gets the callback corresponding to the given string trigger.
		/// </summary>
		/// <param name="triggerToFind">The given trigger corresponding to the callback to find.</param>
		/// <param name="callbackData">It will contain the callback if found, <c>null</c> otherwise.</param>
		/// <returns><c>true</c> if the callback was found, <c>false</c> otherwise.</returns>
		protected bool FindCallback(string triggerToFind, out CallbackContainer callbackData)
		{
			callbackData = _callbacks.Find((cc) => cc.LongTrigger == triggerToFind);
			return callbackData != null;
		}


		/// <summary>
		/// Gets the callback corresponding to the given char trigger.
		/// </summary>
		/// <param name="triggerToFind">The given trigger corresponding to the callback to find.</param>
		/// <param name="callbackData">It will contain the callback if found, <c>null</c> otherwise.</param>
		/// <returns><c>true</c> if the callback was found, <c>false</c> otherwise.</returns>
		protected bool FindCallback(char triggerToFind, out CallbackContainer callbackData)
		{
			callbackData = _callbacks.Find((cc) => cc.ShortTrigger == triggerToFind);
			return callbackData != null;
		}


		#region Edit callback collection methods
		/// <summary>
		/// Adds a new callback with the relative triggers, taking the specified number of options.
		/// </summary>
		/// <param name="callback">The callback to execute when a trigger is found.</param>
		/// <param name="numOfOptions">The number of arguments the callback requires from the array.</param>
		/// <param name="shortTrigger">The single char trigger that raises the callback.</param>
		/// <param name="longTrigger">The multi char, string trigger that raises the callback.</param>
		/// <param name="invokeAsync">If set to <c>true</c>, the callback will be invoked asyncronuosly, syncronously otherwise.</param>
		public void AddCallback(Action<string[]> callback, int numOfOptions, char shortTrigger, string longTrigger, bool invokeAsync)
		{
			CheckNumOfOptions(numOfOptions);
			CheckShortTrigger(shortTrigger);
			CheckLongTrigger(longTrigger);

			_callbacks.Add(new CallbackContainer(callback, numOfOptions, shortTrigger, longTrigger, invokeAsync));
		}


		/// <summary>
		/// Adds a new callback with the relative triggers, taking all the options until the next argument.
		/// </summary>
		/// <param name="callback">The function to call when a trigger is found.</param>
		/// <param name="anyoptions">If set to <c>true</c>, the callback accepts an unlimited number of options, none otherwise.</param>
		/// <param name="shortTrigger">The single char trigger that raises the callback.</param>
		/// <param name="longTrigger">The multi char, string trigger that raises the callback.</param>
		/// <param name="invokeAsync">If set to <c>true</c>, the callback will be invoked asyncronuosly, syncronously otherwise.</param>
		public void AddCallback(Action<string[]> callback, bool anyoptions, char shortTrigger, string longTrigger, bool invokeAsync)
		{
			CheckShortTrigger(shortTrigger);
			CheckLongTrigger(longTrigger);

			int paramNum = anyoptions ? CallbackContainer.Alloptions : 0;

			_callbacks.Add(new CallbackContainer(callback, paramNum, shortTrigger, longTrigger, invokeAsync));
		}


		/// <summary>
		/// Adds a new callback with the relative triggers, taking the specified number of options or until the next argument.
		/// </summary>
		/// <param name="callback">The callback to execute when a trigger is found.</param>
		/// <param name="numOfOptions">The number of arguments the callback requires from the array.</param>
		/// <param name="shortTrigger">The single char trigger that raises the callback.</param>
		/// <param name="invokeAsync">If set to <c>true</c>, the callback will be invoked asyncronuosly, syncronously otherwise.</param>
		public void AddCallback(Action<string[]> callback, int numOfOptions, char shortTrigger, bool invokeAsync)
		{
			CheckNumOfOptions(numOfOptions);
			CheckShortTrigger(shortTrigger);

			_callbacks.Add(new CallbackContainer(callback, numOfOptions, shortTrigger, CallbackContainer.NoLongTrigger, invokeAsync));
		}


		/// <summary>
		/// Adds a new callback with the relative triggers, taking the specified number of options or until the next argument.
		/// </summary>
		/// <param name="callback">The callback to execute when a trigger is found.</param>
		/// <param name="anyoptions">If set to <c>true</c>, the callback accepts an unlimited number of options, none otherwise.</param>
		/// <param name="shortTrigger">The single char trigger that raises the callback.</param>
		/// <param name="invokeAsync">If set to <c>true</c>, the callback will be invoked asyncronuosly, syncronously otherwise.</param>
		public void AddCallback(Action<string[]> callback, bool anyoptions, char shortTrigger, bool invokeAsync)
		{
			CheckShortTrigger(shortTrigger);

			int paramNum = anyoptions ? CallbackContainer.Alloptions : 0;

			_callbacks.Add(new CallbackContainer(callback, paramNum, shortTrigger, CallbackContainer.NoLongTrigger, invokeAsync));
		}


		/// <summary>
		/// Adds a new callback with the relative triggers, taking the specified number of options or until the next argument.
		/// </summary>
		/// <param name="callback">The callback to execute when a trigger is found.</param>
		/// <param name="numOfOptions">The number of arguments the callback requires from the array.</param>
		/// <param name="longTrigger">The multi char, string trigger that raises the callback.</param>
		/// <param name="invokeAsync">If set to <c>true</c>, the callback will be invoked asyncronuosly, syncronously otherwise.</param>
		public void AddCallback(Action<string[]> callback, int numOfOptions, string longTrigger, bool invokeAsync)
		{
			CheckNumOfOptions(numOfOptions);
			CheckLongTrigger(longTrigger);

			_callbacks.Add(new CallbackContainer(callback, numOfOptions, CallbackContainer.NoShortTrigger, longTrigger, invokeAsync));
		}


		/// <summary>
		/// Adds a new callback with the relative triggers, taking the specified number of options or until the next argument.
		/// </summary>
		/// <param name="callback">The callback to execute when a trigger is found.</param>
		/// <param name="anyoptions">If set to <c>true</c>, the callback accepts an unlimited number of options, none otherwise.</param> 
		/// <param name="longTrigger">The multi char, string trigger that raises the callback.</param>
		/// <param name="invokeAsync">If set to <c>true</c>, the callback will be invoked asyncronuosly, syncronously otherwise.</param>
		public void AddCallback(Action<string[]> callback, bool anyoptions, string longTrigger, bool invokeAsync)
		{
			CheckLongTrigger(longTrigger);

			int paramNum = anyoptions ? CallbackContainer.Alloptions : 0;

			_callbacks.Add(new CallbackContainer(callback, paramNum, CallbackContainer.NoShortTrigger, longTrigger, invokeAsync));
		}


		/// <summary>
		/// Removes all the callbacks identified by the trigger.
		/// </summary>
		/// <param name="triggerToRemove">Trigger identifying the callback to remove.</param>
		/// <returns>Returns true if the callback is succefully removed, false if the trigger can't be found.</returns>
		public int RemoveAllCallbacks(string triggerToRemove) { return _callbacks.RemoveAll((cc) => cc.LongTrigger == triggerToRemove); }


		/// <summary>
		/// Removes all the callbacks identified by the trigger.
		/// </summary>
		/// <param name="triggerToRemove">Trigger identifying the callback to remove.</param>
		/// <returns>Returns true if the callback is succefully removed, false if the trigger can't be found.</returns>
		public int RemoveAllCallbacks(char triggerToRemove) { return _callbacks.RemoveAll((cc) => cc.ShortTrigger == triggerToRemove); }


		/// <summary>
		/// Removes all the callbacks identified by the triggers. 
		/// </summary>
		/// <param name="shortTriggerToRemove">Short trigger identifying the callback to remove.</param>
		/// <param name="longTriggerToRemove">Long trigger identifying the callback to remove.</param>
		/// <returns>Returns true if the callback is succefully removed, false if the trigger can't be found.</returns>
		public int RemoveAllCallbacks(char shortTriggerToRemove, string longTriggerToRemove)
		{
			return _callbacks.RemoveAll((cc) => cc.ShortTrigger == shortTriggerToRemove && cc.LongTrigger == longTriggerToRemove);
		}


		/// <summary>
		/// Removes all the callbacks which match the given expression.
		/// </summary>
		/// <param name="matcher">Method to identify all the callbacks to remove.</param>
		/// <returns>Returns true if the callback is succefully removed, false if the trigger can't be found.</returns>
		public int RemoveAllCallbacks(Func<char, string, int, bool> matcher)
		{
			return _callbacks.RemoveAll((cc) => matcher(cc.ShortTrigger, cc.LongTrigger, cc.NumOfOptions));
		}
		#endregion


		/// <summary>
		/// Raises the given callbacks.
		/// </summary>
		/// <param name="callbackContainers">The callback containers to call.</param>
		/// <param name="optionsBuffer">The options to give to the arguments.</param>
		/// <returns>Unused arguments.</returns>
		protected IList<string> Invoke(List<CallbackContainer> callbackContainers, List<string> optionsBuffer)
		{
			if(callbackContainers.Count > 0)
			{
				string[] options = null;
				int max = int.MinValue;

				// Raises the previous callbacks.
				foreach(CallbackContainer container in callbackContainers)
				{
					if(container.NumOfOptions == CallbackContainer.Alloptions)
					{
						options = optionsBuffer.ToArray();
						max = int.MaxValue;
					}
					else
					{
						// Only the requested number of options are given.
						options = new string[container.NumOfOptions];
						optionsBuffer.CopyTo(0, options, 0, container.NumOfOptions);

						if(container.NumOfOptions > max)
							max = container.NumOfOptions;
					}

					Action<string[]> callback = container.Callback;

					// Raise the callback
					if(container.InvokeAsync)
						callback.BeginInvoke(options, (ar) => callback.EndInvoke(ar), null);
					else
						callback.Invoke(options);
				}

				if(max < optionsBuffer.Count)
				{
					// Extracts the unused options
					int count = optionsBuffer.Count - max;
					string[] unusedoptions = new string[count];

					optionsBuffer.CopyTo(max, unusedoptions, 0, count);
					return unusedoptions;
				}
				else
					return new string[0];
			}
			else
				return optionsBuffer;
		}


		#region Public interface
		/// <summary>
		/// Parses a given array of argumetns an calls the relative callback when the relative trigger is found.
		/// </summary>
		/// <returns>The strings not identified as triggers or used ar arguments.</returns>
		/// <param name="args">The array to search in.</param>
		/// <param name="firstTriggerOnly">If set to <c>true</c>, only the first trigger found will be called and everything else will be given as options to that option.</param>
		/// <param name="args">The array to search in.</param>
		public string[] ParseSequence(IEnumerable<string> args, bool firstTriggerOnly = false, OnUnknowTrigger unknowTriggerAction = OnUnknowTrigger.None)
		{
			// Buffer to contain the options of the arguments.
			List<string> optionsBuffer = new List<string>();
			// Contains all the options remained unused by the arguments.
			List<string> unusedParamBuffer = new List<string>();
			// The latest fetched callbacks. Only single triggers should be allowed to accumulate.
			List<CallbackContainer> lastCallbackContainers = new List<CallbackContainer>();
			// Useful only if first trigger only is enabled.
			bool firstTriggerFound = false;


			foreach(string argument in args)
			{
				ArgumentType argumentType = GetArgumentType(argument);

				if((firstTriggerOnly && firstTriggerFound) || argumentType == ArgumentType.option)
					// Didn't find any callback, or is set to invoke the first and
					// only trigger: the string is added to the parametes buffer.
					optionsBuffer.Add(argument);
				else
				{
					// Removes eventual separators.
					string arg = StripSeparator(argument, argumentType);

					CallbackContainer lastCallbackContainer;

					if(argumentType == ArgumentType.ShortTrigger)
						foreach(char c in arg)
						{
							// Short triggers can be in groups (ex: -aBc), so we let them accumulate and don't call the callbacks.
							if(FindCallback(c, out lastCallbackContainer))
								lastCallbackContainers.Add(lastCallbackContainer);
							else
							{
								if(unknowTriggerAction == OnUnknowTrigger.AsUnused)
									unusedParamBuffer.Add(c.ToString());
								else if(unknowTriggerAction == OnUnknowTrigger.Throw)
									throw new ArgumentException("Unknow short trigger '" + c + "'.");
							}
						}
					else
					{
						// Calls the previouses callbacks, using the buffered arguments.
						IList<string> unusedoptions = Invoke(lastCallbackContainers, optionsBuffer);
						unusedParamBuffer.AddRange(unusedoptions);

						// Clears previous options and arguments.
						lastCallbackContainers.Clear();
						optionsBuffer.Clear();


						// Loads the new callback.
						if(FindCallback(arg, out lastCallbackContainer))
							lastCallbackContainers.Add(lastCallbackContainer);
						else
						{
							if(unknowTriggerAction == OnUnknowTrigger.AsUnused)
								unusedParamBuffer.Add(arg);
							else if(unknowTriggerAction == OnUnknowTrigger.Throw)
								throw new ArgumentException("Unknow long trigger '" + arg + "'.");
						}
					}

					// At least the first trigger has been found.
					firstTriggerFound = true;
				}				
			}

			// Calls the last callbacks with the fetched options.
			unusedParamBuffer.AddRange(Invoke(lastCallbackContainers, optionsBuffer));

			return unusedParamBuffer.ToArray();
		}


		/// <summary>
		/// Asyncronously parses a given enumeration of triggers an calls the relative callback when the relative trigger is found.
		/// </summary>
		/// <returns>The strings not identified as triggers or used ar arguments.</returns>
		/// <param name="args">The enumeration to search triggers in.</param>
		public Task<string[]> ParseSequenceAsync(IEnumerable<string> args, bool firstTriggerOnly = false, OnUnknowTrigger unknowTriggerAction = OnUnknowTrigger.None)
		{
			return Task.Factory.StartNew(delegate() { return ParseSequence(args, firstTriggerOnly, unknowTriggerAction); });
		}


		/// <summary>
		/// Clones the current instance.
		/// </summary>
		/// <returns>A clone of the current object.</returns>
		public object Clone()
		{
			CallbackContainer[] callbacksCopy = Cloner.EnumerationClone(_callbacks);

			return new ArgsParser(UsingWindowsSlash, callbacksCopy.ToList());
		}
		#endregion
	}
}
