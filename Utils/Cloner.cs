﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Script.Serialization;

/*
	This file is part of the Mek-lib

	Mek-lib is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Mek-lib is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with the Mek-lib.If not, see<http://www.gnu.org/licenses/>.
*/

namespace Mek_lib.Utils
{
	/// <summary>
	/// Allows to deep clone objects.
	/// </summary>
	public static class Cloner
	{
		private static JavaScriptSerializer _jsonSerializer = new JavaScriptSerializer();

		/// <summary>
		/// Clones a <typeparamref name="T"/> object.
		/// The clone is equal only by PUBLIC fields.
		/// </summary>
		/// <returns>The clone.</returns>
		/// <param name="obj">Object to clone.</param>
		/// <typeparam name="T">The type of the object to clone parameter.</typeparam>
		public static T PublicClone<T>(T obj)
		{
			if(typeof(T).IsByRef)
			{
				if(!typeof(T).IsSerializable)
					throw new ArgumentException("The object is not serializable.");

				if(object.ReferenceEquals(obj, default(T)))
					return default(T);

				string serialized = _jsonSerializer.Serialize(obj);
				return _jsonSerializer.Deserialize<T>(serialized);
			}
			else
				// No need to copy a value type.
				return obj;
		}


		/// <summary>
		/// Performs a deep copy of all fields.
		/// </summary>
		/// <returns>The clone.</returns>
		/// <param name="obj">Object.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T FullClone<T>(T obj)
		{
			if(typeof(T).IsByRef)
			{
                object clone = null;

                if (object.ReferenceEquals(obj, default(T)))
					clone = default(T);                
				// Tries to use "native" cloning.
				else if(typeof(T) is ICloneable)
					clone = (T)((ICloneable)obj).Clone();
				else
				{
					if(!typeof(T).IsSerializable)
						throw new ArgumentException("The object is not serializable.");
					
					BinaryFormatter formatter = new BinaryFormatter();

					using(MemoryStream stream = new MemoryStream())
					{
						formatter.Serialize(stream, obj);
						stream.Seek(0, SeekOrigin.Begin);
						clone = formatter.Deserialize(stream);
					}					
				}

                return (T)clone;
            }
			else
				// No need to copy a value type.
				return obj;
		}


		/// <summary>
		/// Creates a clone of the given dictionary.
		/// </summary>
		/// <returns>The clone.</returns>
		/// <param name="dictionary">Dictionary to clone..</param>
		/// <typeparam name="K">The <paramref name="dictionary"/> Key type parameter.</typeparam>
		/// <typeparam name="V">The <paramref name="dictionary"/> Value type parameter.</typeparam>
		public static Dictionary<K, V> DictionaryClone<K, V>(IDictionary<K, V> dictionary) { return dictionary.ToDictionary((kv) => FullClone(kv.Key), (kv) => FullClone(kv.Value)); }


		/// <summary>
		/// Creates a clone of the given enumeration.
		/// </summary>
		/// <returns>The clone.</returns>
		/// <param name="enumeration">The source enumeration.</param>
		/// <typeparam name="T">The <paramref name="enumeration"/> type parameter.</typeparam>
		public static T[] EnumerationClone<T>(IEnumerable<T> enumeration)
		{
			T[] result = new T[enumeration.Count()];

			for(int i = 0; i < result.Length; i++)
				result[i] = FullClone(enumeration.ElementAt(i));

			return result;
		}
	}
}
