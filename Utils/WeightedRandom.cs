using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

/*
	This file is part of the Mek-lib

	Mek-lib is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Mek-lib is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with the Mek-lib.If not, see<http://www.gnu.org/licenses/>.
*/

namespace Mek_lib.Utils
{
	/// <summary>
	/// Pool of <typeparamref name="T"/> elements with a weight. Allows to extract a random element from the sequence according to it's weight.
	/// </summary>
	public class WeightedRandom<T> : IEnumerable<KeyValuePair<T, float>>, ICloneable
	{
		#region Fields
		/// <summary>
		/// Holds a sequence of values with their extraction chances.
		/// </summary>
		/// <value>The pair of element and percentage of extraction.</value>
		public IDictionary<T, float> _pairs;


		/// <summary>
		/// Total chances of all the items.
		/// </summary>
		/// <value>The probabilities.</value>
		public float Probabilities { private set; get; }


		/// <summary>
		/// Counts the number of elements contained in this instance.
		/// </summary>
		public int Count { get { return _pairs.Count; } }


		/// <summary>
		/// Access to the percentage of a given element
		/// </summary>
		public float this[T element]
		{
			get { return _pairs[element]; }
			set
			{
				Probabilities -= _pairs[element];
				Probabilities += value;
				_pairs[element] = value;
			}
		}


		// Provides a random value
		private Random _randomizer;
		#endregion


		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="T:Mek_lib.Utils.WeightedRandom`1"/> class.
		/// </summary>
		public WeightedRandom() : this(new Dictionary<T, float>()) { }


		/// <summary>
		/// Initializes a new instance of the <see cref="T:Mek_lib.Utils.WeightedRandom`1"/> class.
		/// </summary>
		/// <param name="sourcePairs">Initial pairs.</param>
		public WeightedRandom(IDictionary<T, float> sourcePairs) : this(sourcePairs, 0) { }


		/// <summary>
		/// Initializes a new instance of the <see cref="T:Mek_lib.Utils.WeightedRandom`1"/> class.
		/// Usefult to clone the instance.
		/// </summary>
		/// <param name="sourcePairs">Initial pairs.</param>
		/// <param name="probabilities">Total probabilities.</param>
		private WeightedRandom(IDictionary<T, float> sourcePairs, float probabilities)
		{
			_pairs = sourcePairs;
			_randomizer = new Random();
			Probabilities = probabilities;
		}
		#endregion


		/// <summary>
		/// Extracts a random element in weighted manner.
		/// </summary>
		/// <param name="element">Place holder to the elment to extract.</param>
		/// <returns><c>true</c> if an elment was extracted, <c>false</c> otherwise.</returns>
		public bool TryGetRandomElement(out T element)
		{
			// The number we want to extract, with 4 decimals
			float extract = _randomizer.Next((int)(Probabilities * 10000)) / 10000;
			float proababilityAccomulator = 0;

			foreach(KeyValuePair<T, float> pair in _pairs)
			{
				// Increases the cumulator
				proababilityAccomulator += pair.Value;

				if(extract <= proababilityAccomulator)
				{
					element = pair.Key;
					return true;
				}
			}

			element = default(T);
			return false;
		}


		#region Collection Edit Methods
		/// <summary>
		/// Proportionaly changes all the weights given a new top.
		/// </summary>
		/// <param name="top">The new total percentage value.</param>
		public void NormilizeAt(float top)
		{
			for(int i = 0; i < _pairs.Count; i++)
				// Horrible hack. but faster than copiying all the keys.
				_pairs[_pairs.ElementAt(i).Key] = _pairs.ElementAt(i).Value * top / Probabilities;

			Probabilities = top;
		}


		/// <summary>
		/// Adds a new element with it's percentage.
		/// </summary>
		/// <param name="element">The element to add.</param>
		/// <param name="weight">The extraction chances related to the element.</param>
		public void Add(T element, float weight)
		{
			Probabilities += weight;
			_pairs.Add(element, weight);
		}


		/// <summary>
		/// Concats the associations.
		/// </summary>
		/// <param name="dictionary">Dictionary.</param>
		public void ConcatAssociations(IDictionary<T, float> dictionary) { _pairs.Concat(dictionary); }


		/// <summary>
		/// Removes the given element.
		/// </summary>
		/// <param name="element">The element to remove.</param>
		/// <returns>Returns true if the element is succefully removed, false otherwise.</returns>
		public bool Remove(T element)
		{
			if(_pairs.Remove(element))
			{
				Probabilities -= _pairs[element];
				return true;
			}
			else
				return false;
		}


		/// <summary>
		/// Returns if the element is inside the internal list.
		/// </summary>
		/// <param name="element">The element to search for.</param>
		/// <returns>True if it's present, False otherwise.</returns>
		public bool Contains(T element) { return _pairs.ContainsKey(element); }


		/// <summary>
		/// Clones the internal dictionary.
		/// </summary>
		/// <returns><see langword="async"/> copy of the current dictionary.</returns>
		public IDictionary<T, float> CloneDictionary() { return _pairs.ToDictionary((k) => k.Key, (v) => v.Value); }
		#endregion


		#region Interfaces Implementation
		public IEnumerator<KeyValuePair<T, float>> GetEnumerator() { return _pairs.GetEnumerator(); }

		IEnumerator IEnumerable.GetEnumerator() { return _pairs.GetEnumerator(); }


		/// <summary>
		/// Returns a full clone this instance.
		/// </summary>
		/// <returns>The clone.</returns>
		public object Clone() { return new WeightedRandom<T>(Cloner.DictionaryClone(_pairs), Probabilities); }
		#endregion

	}
}
