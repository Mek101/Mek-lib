﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Mek_lib.Algorithms
{
	internal sealed class MarkovEnumerable<T> : IEnumerable<T>
	{
		private readonly Markov<T> _collection;
		private readonly T _startElement;
		private readonly bool _finite;
		private readonly int _min;
		private readonly int _max;

		#region No start element contructors
		public MarkovEnumerable(Markov<T> collection, int min, int max = -1) : this(collection, collection.GetElement(), min, max) { }

		public MarkovEnumerable(Markov<T> collection, bool infinite) : this(collection, collection.GetElement(), infinite) { }

		#endregion

		#region Start element contructors
		public MarkovEnumerable(Markov<T> collection, T startElement, int min, int max = -1)
		{
			if(min < 0 || (max != -1 && max < 0))
				throw new ArgumentOutOfRangeException("", "The enumeration size cannot be bellow 0!");

			if(max < min)
				throw new ArgumentOutOfRangeException(nameof(max), "The maximum size cannot be inferior to the minimum size!");

			_collection = collection;
			_startElement = startElement;
			_finite = true;
			_min = min;
			_max = max;
		}


		public MarkovEnumerable(Markov<T> collection, T startElement, bool infinite)
		{
			_collection = collection;
			_startElement = startElement;
			_finite = infinite;
			_min = -1;
			_max = -1;
		}
		#endregion

		public IEnumerator<T> GetEnumerator() => new MarkovEnumerator(_collection, _startElement, _finite, _min, _max);

		IEnumerator IEnumerable.GetEnumerator() => new MarkovEnumerator(_collection, _startElement, _finite, _min, _max);

		#region Enumerator
		/// <summary>
		/// Nested nested class, Markov enumerator.
		/// </summary>
		private sealed class MarkovEnumerator : IEnumerator<T>
		{
			private Markov<T> _collection;
			private readonly bool _finite;
			private readonly int _min;
			private readonly int _max;

			int _currentSize;
			T _current;


			public MarkovEnumerator(Markov<T> collection, T startElement, bool finite, int min, int max)
			{
				_collection = collection;
				_finite = finite;
				_min = min;
				_max = max;
				_currentSize = 0;
				_current =  startElement;
			}

			private bool IndeterminatedFinite => _min == -1 && _max == -1;

			public T Current => _current;


			object IEnumerator.Current => _current;


			public bool MoveNext()
			{
				if(_finite)
				{
					if(IndeterminatedFinite || _currentSize < _min || !(_max != -1 && _currentSize < _max))
					{
						T nextElement;
						bool found = _collection.TryGetNextElement(_current, out nextElement);

						if(found)
						{
							_current = nextElement;
							_currentSize++;
						}

						return found;
					}
					else
						return false;
				}
				else
				{
					_current = _collection.GetNextElement(_current);
					return true;
				}
			}


			public void Reset()
			{
				_currentSize = 0;
				_current = _collection.GetElement();
			}


			#region IDisposable Support
			// Questo codice viene aggiunto per implementare in modo corretto il criterio Disposable.
			public void Dispose() { _collection = null; }
			#endregion
		}
		#endregion
	}
}
