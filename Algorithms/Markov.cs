﻿using Mek_lib.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

/*
    This file is part of the Mek-lib

    Mek-lib is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mek-lib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the Mek-lib. If not, see<http://www.gnu.org/licenses/>.
*/
namespace Mek_lib.Algorithms
{
	/// <summary>
	/// Givan a sequeunce of T elements, process them with the Markov algorithm and returns a new sequence.
	/// For every given element, it's checked by which element is followed and how many times.
	/// The new sequence is built appending a semi-random element from the ones who followed the actual one.
	/// </summary>
	public class Markov<T> : ICloneable
	{
		#region Fields
		// Offers a random number.
		protected readonly Random _randomizer;

		/// <summary>
		/// Gets the internal association dictionary.
		/// </summary>
		/// <value>The association dictionary.</value>
		public IDictionary<T, WeightedRandom<T>> AssociationsDictionary { protected set; get; }
		#endregion


		#region Constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="T:Mek_lib.Algorithms.Markov`1"/>, then loads a sequence.
		/// </summary>
		/// <param name="baseSequence">The original sequence to analyze.</param>
		public Markov(IEnumerable<T> baseSequence) : this() { LoadSequence(baseSequence); }


		/// <summary>
		/// Initializes a new instance of the <see cref="T:Mek_lib.Algorithms.Markov`1"/>.
		/// </summary>
		public Markov() : this(new Dictionary<T, WeightedRandom<T>>()) { }


		/// <summary>
		/// Initializes a new instance of the <see cref="T:Mek_lib.Algorithms.Markov`1"/> class.
		/// </summary>
		/// <param name="sourceDictionary">Source dictionary containing the initial associations.</param>
		public Markov(IDictionary<T, WeightedRandom<T>> sourceDictionary)
		{
			AssociationsDictionary = new Dictionary<T, WeightedRandom<T>>();
			_randomizer = new Random();
		}
		#endregion


		#region Private Methods
		/// <summary>
		/// Given a array of elements, returns the elemets from a starting index and the number of times they appear.
		/// The start index is NOT included.
		/// </summary>
		/// <param name="sequence">The sequence of elements to analyze.</param>
		/// <param name="startIndex">The start index to analyze the sequence.</param>
		/// <returns>Weighted array containg the elements and their percentage.</returns>
		protected WeightedRandom<T> CountItems(IEnumerable<T> sequence, int startIndex)
		{
			WeightedRandom<T> elementAssociator = new WeightedRandom<T>();

			if (startIndex < sequence.Count())
				for (int i = startIndex + 1; i < sequence.Count(); i++)
				{
					T currentElement = sequence.ElementAt(i);

					// If the element was already encountred
					if (elementAssociator.Contains(currentElement))
						// Increments the frequency
						elementAssociator[currentElement]++;
					else
						// Adds the new element otherwise
						elementAssociator.Add(currentElement, 1);
				}

			return elementAssociator;
		}
		#endregion


		#region Sequence Methods
		/// <summary>
		/// Analyzes the elemenets and their frequency in a given sequence.
		/// </summary>
		/// <param name="sequence">A sequence of elements to analyze.</param>
		public void LoadSequence(IEnumerable<T> sequence)
		{
			for (int i = 0; i < sequence.Count(); i++)
			{
				T currentElement = sequence.ElementAt(i);

				// If it was already added
				if (AssociationsDictionary.ContainsKey(currentElement))
				{
					WeightedRandom<T> weightedAssociator = CountItems(sequence, i);

					foreach (KeyValuePair<T, float> element in weightedAssociator)
					{
						// If there's already an association for that entry
						if (AssociationsDictionary[currentElement].Contains(element.Key))
							// Increases the value
							AssociationsDictionary[currentElement][element.Key] += element.Value;
						else
							// Adds a new association otherwise
							AssociationsDictionary[currentElement].Add(element.Key, element.Value);
					}
				}
				else
				{
					// Adds it to the association dictionary, including the elements that follows and their frequency
					AssociationsDictionary.Add(currentElement, CountItems(sequence, i));
				}
			}
		}


		/// <summary>
		/// Extracts a new sequence from the previous sequence, chosen and sorted through the makrov algorithm.
		/// </summary>
		/// <param name="sequenceLentgh">The maxiumum lentgh of the array.</param>
		/// <returns>A new seqeunce of T elements.</returns>
		public List<T> GetSequence(int sequenceLentgh) => GetSequence(0, sequenceLentgh);


		/// <summary>
		/// Extracts a new sequence from the previous sequence, chosen and sorted through the makrov algorithm.
		/// </summary>
		/// <param name="currentElement">The element to start from.</param>
		/// <param name="sequenceLentgh">The maxiumum lentgh of the array.</param>
		/// <returns>A new seqeunce of T elements.</returns>
		public List<T> GetSequence(T currentElement, int sequenceLentgh) => GetSequence(currentElement, 0, sequenceLentgh);


		/// <summary>
		/// Extracts a new sequence from the previous sequence, chosen and sorted throug the makrov algorithm.
		/// </summary>
		/// <param name="minSequenceLength">The minimum lentgh of the array.</param>
		/// <param name="maxSequenceLength">The maxiumum lentgh of the array.</param>
		/// <returns>A new seqeunce of T elements.</returns>
		public List<T> GetSequence(int minSequenceLength, int maxSequenceLength) => GetSequence(GetElement(), minSequenceLength, maxSequenceLength);


		/// <summary>
		/// Extracts a new sequence from the previous sequence, chosen and sorted through the makrov algorithm.
		/// </summary>
		/// <param name="startElement">The element to start from.</param>
		/// <param name="minSequenceLength">The minimum lentgh of the array.</param>
		/// <param name="maxSequenceLength">The maxiumum lentgh of the array.</param>
		/// <returns>A new seqeunce of T elements.</returns>
		public List<T> GetSequence(T startElement, int minSequenceLength, int maxSequenceLength)
		{
			return new List<T>(EnumerateFrom(startElement, minSequenceLength, maxSequenceLength));
		}


		/// <summary>
		/// Generates a enumerable sequence of <typeparamref name="T"/> elements.
		/// </summary>
		/// <returns>An enumeration of generated elements</returns>
		/// <param name="finite">If set to <c>true</c> the enumeration will last until a new element can not be found..</param>
		public IEnumerable<T> EnumerateAs(bool finite)
		{
			return new MarkovEnumerable<T>(this, finite);
		}

		public IEnumerable<T> EnumerateAs(int min, int max)
		{
			return new MarkovEnumerable<T>(this, min, max);
		}

		public IEnumerable<T> EnumerateMinimum(int lentgh)
		{
			return new MarkovEnumerable<T>(this, lentgh);
		}

		public IEnumerable<T> EnumerateMaximum(int lentgh)
		{
			return new MarkovEnumerable<T>(this, lentgh, lentgh);
		}

		public IEnumerable<T> EnumerateFrom(T element, bool finite)
		{
			return new MarkovEnumerable<T>(this, element, finite)
		}

		public IEnumerable<T> EnumerateFrom(T element, int min, int max)
		{
			return new MarkovEnumerable<T>(this, element, min, max);
		}

		public IEnumerable<T> EnumerateMinimum(T element, int lentgh)
		{
			return new MarkovEnumerable<T>(this, element, lentgh);
		}

		public IEnumerable<T> EnumerateMaximum(T element, int lentgh)
		{
			return new MarkovEnumerable<T>(this, element, lentgh, lentgh);
		}
		#endregion


		#region Single possible element deterministic extraction
		/// <summary>
		/// Tries the extract a following element.
		/// </summary>
		/// <returns><c>true</c>, if get next element was extracted, <c>false</c> if none could be found.</returns>
		/// <param name="sourceElement">Source element.</param>
		/// <param name="nextElement">Next element place holder.</param>
		public bool TryGetNextElement(T sourceElement, out T nextElement)
		{
			if(AssociationsDictionary.ContainsKey(sourceElement))
			{
				if(AssociationsDictionary[sourceElement].TryGetRandomElement(out nextElement))
					return true;
			}

			nextElement = default(T);
			return false;
		}
		#endregion


		#region Single sure element extraction
		/// <summary>
		/// Extracts a following element.
		/// </summary>
		/// <returns>A element following the given one.</returns>
		/// <param name="element">Source element.</param>
		public T GetNextElement(T element)
		{
			if(AssociationsDictionary.ContainsKey(element))
			{
				T nextElement;
				if(AssociationsDictionary[element].Count == 0)
					return GetElement();
				else
				{
					while(!AssociationsDictionary[element].TryGetRandomElement(out nextElement)) { }

					return nextElement;
				}
			}
			else
				throw new KeyNotFoundException("Unknow source " + element.ToString() + " element.");
		}


		/// <summary>
		/// Extracts a random element from the assosiaction dictionary.
		/// </summary>
		/// <returns>A random element.</returns>
		public T GetElement()
		{
			return AssociationsDictionary.ElementAt(_randomizer.Next(0, AssociationsDictionary.Count)).Key;
		}
		#endregion


		#region Collection Edit Methods
		/// <summary>
		/// Concatenates the associations.
		/// </summary>
		/// <param name="dictionary">Dictionary.</param>
		public void Concat(IDictionary<T, WeightedRandom<T>> dictionary) { AssociationsDictionary.Concat(dictionary); }


		/// <summary>
		/// Clones the internal associations dictionary.
		/// </summary>
		/// <returns>The associations dictionary.</returns>
		public Dictionary<T, WeightedRandom<T>> CloneAssociationsDictionary() { return Cloner.DictionaryClone(AssociationsDictionary); }


		/// <summary>
		/// Returns if there's the specified element inside the associaction dictionary.
		/// </summary>
		/// <param name="element">The element to search.</param>
		/// <returns>True if the given elment exists in the original sequence, false otherwise.</returns>
		public bool Contains(T element) { return AssociationsDictionary.ContainsKey(element); }


		/// <summary>
		/// Removes all the data in sequence.
		/// </summary>
		public void ClearSequence() { AssociationsDictionary.Clear(); }


		/// <summary>
		/// Clone this instance.
		/// </summary>
		/// <returns>A copy of the current object.</returns>
		public object Clone() { return new Markov<T>(CloneAssociationsDictionary()); }
		#endregion
	}
}
