using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Mek_lib.Utils.IO
{
	/// <summary>
	/// Transforms a <typeparamref name="T"/> object in xml and back.
	/// </summary>
	public class XmlFormatter<T> : Formatter<T>
	{
		protected XmlSerializer _serializer;

		/// <summary>
		/// Creates a new xml formatter instance.
		/// </summary>
		public XmlFormatter() : this(typeof(T).ToString().ToLower()) { }


		/// <summary>
		/// Creates a new xml formatter instance with the given root tag.
		/// </summary>
		/// <param name="formatRoot">Specifies a custom <seealso cref="XmlRootAttribute"/>.</param>
		public XmlFormatter(string formatRoot)
		{
			_serializer = new XmlSerializer(typeof(T), new XmlRootAttribute(formatRoot));
		}


		/// <summary>
		/// Gets the default file extension.
		/// </summary>
		/// <value>The default file extension.</value>
		public override string DefaultFileExtension { get { return "xml"; } }


		/// <summary>
		/// Transforms the given <typeparamref name="T"/> object to xml byte array.
		/// </summary>
		/// <param name="objectToSerialize">The <typeparamref name="T"/> object to serialize.</param>
		/// <returns>The object in a serialized, byte form.</returns>
		public override byte[] Serialize(T objectToSerialize)
		{
			// Creates a buffer.
			MemoryStream buffer = new MemoryStream();

			// Writes the xml formated text in the buffer.
			_serializer.Serialize(buffer, objectToSerialize);

			return buffer.ToArray();
		}


		/// <summary>
		/// Transforms the given <typeparamref name="T"/> object to a Xml formatted string
		/// </summary>
		/// <param name="objectToSerialize">The T object to serialize</param>
		/// <returns>The object in a serialized, string form</returns>
		public string SerializeToString(T objectToSerialize)
		{
			// Encodes the byte array to a string
			return Encoding.UTF32.GetString(Serialize(objectToSerialize));
		}


		/// <summary>
		/// Given an stream, deserializes it in xml using XmlSerialize and creates the corresponding <typeparamref name="T"/> object
		/// </summary>
		/// <param name="inStream">The xml source to extract the <typeparamref name="T"/> object from</param>
		/// <returns>The object built from the stream</returns>
		public override T Deserialize(Stream inStream)
		{
			T obj = (T)_serializer.Deserialize(inStream);

			return obj;
		}
	}
}
