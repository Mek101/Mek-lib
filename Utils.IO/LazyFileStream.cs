﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Mek_lib.Utils.IO
{
	/*
     * Forked from: https://gist.github.com/MrZoidberg/3879152
     *
       This file is part of the Mek-lib

       Mek-lib is free software: you can redistribute it and/or modify
       it under the terms of the GNU General Public License as published by
       the Free Software Foundation, either version 3 of the License, or
       (at your option) any later version.

       Mek-lib is distributed in the hope that it will be useful,
       but WITHOUT ANY WARRANTY; without even the implied warranty of
       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
       GNU General Public License for more details.

       You should have received a copy of the GNU General Public License
       along with the Mek-lib.  If not, see <http://www.gnu.org/licenses/>.
    */

	public static class LazyFileStream
	{
		// Defines the constant value.
		const int ERROR_SHARING_VIOLATION = 32;

		/// <summary>
		/// Opens the given file waiting it's free.
		/// </summary>
		/// <param name="path">The file path.</param>
		/// <param name="mode">The file opening mode.</param>
		/// <param name="fileAccess">The file access to set.</param>
		/// <param name="fileShare">File sharing to set.</param>
		/// <returns>A filestream.</returns>
		public static FileStream LazyGetFileStream(string path, FileMode mode = FileMode.Create, FileAccess fileAccess = FileAccess.Read, FileShare fileShare = FileShare.None)
		{
			// Checks if the path is valid.
			if(string.IsNullOrEmpty(path))
				throw new ArgumentNullException(nameof(path), "The specified path cannot be null.");

			// Creates a new filestream watcher.
			FileSystemWatcher fileSystemWatcher = new FileSystemWatcher(Path.GetDirectoryName(path), "*" + Path.GetExtension(path));

			// Creates a new autoreset event.
			AutoResetEvent autoResetEvent = new AutoResetEvent(false);


			// Sets a response to the status of the file changing.
			fileSystemWatcher.Changed += delegate (object sender, FileSystemEventArgs e)
			{
				// It's the specified file to have changed.
				if(Path.GetFullPath(e.FullPath) == Path.GetFullPath(path))
					// Raises the event, unlocking the loop.
					autoResetEvent.Set();
			};
			// Enables the event raising
			fileSystemWatcher.EnableRaisingEvents = true;

			FileStream stream = null;


			// Loops, trying to open the stream.
			while(true)
			{
				// Tries to open the stream.
				try
				{
					stream = new FileStream(path, mode, fileAccess, fileShare);

					autoResetEvent.Dispose();
					return stream;
				}
				// If there's an IOException.
				catch(IOException e)
				{
					// And it's caused by someone who has locked the access to the file.
					if(ERROR_SHARING_VIOLATION == Marshal.GetLastWin32Error())
					{
						// Waits for the event, blocking the current thread.
						autoResetEvent.WaitOne();
					}
					// If it was something else, the exception is re-thrown.
					else
						throw e;
				}
			}
		}


		/// <summary>
		/// Asyncronously opens the given file waiting it's free.
		/// </summary>
		/// <param name="path">The file path.</param>
		/// <param name="mode">The file opening mode.</param>
		/// <param name="fileAccess">The file access to set.</param>
		/// <param name="fileShare">File sharing to set.</param>
		/// <returns>A filestream.</returns>
		public static Task<FileStream> LazyGetFileStreamAsync(string path, FileMode mode = FileMode.Create, FileAccess fileAccess = FileAccess.Read, FileShare fileShare = FileShare.None)
		{
			return Task.Factory.StartNew<FileStream>(delegate() { return LazyGetFileStream(path, mode, fileAccess, fileShare); });
		}


	}
}
