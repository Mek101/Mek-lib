﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

/*
	This file is part of the Mek-lib

	Mek-lib is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Mek-lib is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with the Mek-lib.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mek_lib.Utils.IO
{
	/// <summary>
	/// Saves and loads asyncronously multiple <typeparamref name="T"/> objects from and to a file in in the given format.
	/// Allows to compression and encryption over those files.
	/// </summary>
	/// <typeparam name="T">The type of the objects to operate on.</typeparam>
	public class ParallelLoader<T>
	{
		#region Data
		/// <summary>
		/// Implements the serialization and deserialization process for the given format
		/// </summary>
		protected Formatter<T> _formatter;

		protected Task[] _pendingTasks;

		protected SymmetricAlgorithm _encriptionProvvider;

		protected string _dirPath;
		protected string _fileExtension;

		protected bool _compression;
		protected readonly bool _encryption;
		#endregion


		#region Constructors
		/// <summary>
		/// Creates a new ParallelLoader instance, enebling encryption on the files.
		/// </summary>
		/// <param name="directoryPath">The path to the directory containing the files.</param>
		/// <param name="key">The encryption key.</param>
		/// <param name="VI">The encryption VI.</param>
		/// <param name="compression">Enables compression.</param>
		public ParallelLoader(string directoryPath, Formatter<T> formatter, byte[] key, byte[] VI, bool compression = false)
			: this(directoryPath, formatter.DefaultFileExtension, formatter, key, VI, compression) { }


		/// <summary>
		/// Creates a new ParallelLoader instance.
		/// </summary>
		/// <param name="directoryPath">The path to the directory containing the files.</param>
		/// <param name="compression">Enables compression.</param>
		public ParallelLoader(string directoryPath, Formatter<T> formatter, bool compression = false)
			: this(directoryPath, formatter.DefaultFileExtension, formatter, compression) { }


		/// <summary>
		/// Creates a new ParallelLoader instance, enebling encryption on the files.
		/// </summary>
		/// <param name="directoryPath">The path to the directory containing the files.</param>
		/// <param name="fileExtension">The files extension.</param>
		/// <param name="key">The encryption key.</param>
		/// <param name="VI">The encryption VI.</param>
		/// <param name="compression">Enables compression.</param>
		public ParallelLoader(string directoryPath, string fileExtension, Formatter<T> formatter, byte[] key, byte[] IV, bool compression = false)
		{
			_encryption = true;

			_encriptionProvvider = Aes.Create();
			_encriptionProvvider.Key = key;
			_encriptionProvvider.IV = IV;

			Init(directoryPath, fileExtension, formatter, compression);
		}


		/// <summary>
		/// Creates a new ParallelLoader instance.
		/// </summary>
		/// <param name="directoryPath">The path to the directory containing the files.</param>
		/// <param name="fileExtension">The files extension.</param>
		/// <param name="compression">Enables compression.</param>
		public ParallelLoader(string directoryPath, string fileExtension, Formatter<T> formatter, bool compression = false)
		{
			_encryption = false;

			Init(directoryPath, fileExtension, formatter, compression);
		}


		// Initializes the shared properties from the constructors.
		protected void Init(string directoryPath, string fileExtension, Formatter<T> formatter, bool compression)
		{
			if(formatter == null)
				throw new ArgumentNullException(nameof(formatter), "The formatter cannot be null.");
			if(IsPathValid(directoryPath))
				throw new ArgumentException("The given path contains invalid characters.", nameof(directoryPath));

			_dirPath = directoryPath;
			_fileExtension = fileExtension;

			_formatter = formatter;
			_compression = compression;

			// By default, the file name randomly selected by Path.
			SetFileName = (o) => Path.GetRandomFileName();
		}


		// Waits for the tasks to terminate before destroying the object.
		~ParallelLoader() { JoinCurrentTasks(); }
		#endregion

		#region Private Methods
		// Tests if the given path is valid.
		protected bool IsPathValid(string path)
		{
			char[] invalidChars = Path.GetInvalidPathChars();

			foreach(char c in invalidChars)
				if(path.Contains(c))
					return false;

			return true;
		}


		// Prints the given Exception.
		protected void PrintError(Exception e) { Console.Error.WriteLine(Task.CurrentId + ": " + e.GetType() + " - " + e.Message); }
		#endregion


		#region Task methods
		/// <summary>
		/// Contains the code executed by tasks to load the cache
		/// </summary>
		/// <param name="container"></param>
		protected void LoadObjectTask(object container)
		{
			Tuple<string, Action<T>> tuple = (Tuple<string, Action<T>>)container;
			string fileName = tuple.Item1;
			Action<T> onObjectReady = tuple.Item2;

			Stream inStream = null;

			try
			{
				// Creates a FileStream for the file
				inStream = LazyFileStream.LazyGetFileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None);

				if(_encryption)
					// Gets a Cryptostream to decrypt the file
					inStream = new CryptoStream(inStream, _encriptionProvvider.CreateDecryptor(), CryptoStreamMode.Read);

				if(_compression)
					// Retrurns a GZipStream to decompress the file
					inStream = new GZipStream(inStream, CompressionMode.Decompress);

				// Calls the method to build the object from the given format, then
				onObjectReady(_formatter.Deserialize(inStream));
			}
			catch(Exception e) { PrintError(e); }
			finally
			{
				if(inStream != null)
					inStream.Dispose();
			}
		}


		/// <summary>
		/// Contains the code executed by tasks to save the cache
		/// </summary>
		/// <param name="container">The object to convert and save to file.</param>
		protected void SaveFileTask(object container)
		{
			T objectToSave = (T)container;

			Stream outStream = null;

			try
			{
				// Creates a FileStream for wri ting the file. The file name it's made from it's hash.
				outStream = LazyFileStream.LazyGetFileStream(Path.Combine(_dirPath, SetFileName(objectToSave) + "." + _fileExtension), FileMode.Create, FileAccess.Write, FileShare.None);

				if(_encryption)
					// Gets a Cryptostream to encrypt the file.
					outStream = new CryptoStream(outStream, _encriptionProvvider.CreateEncryptor(), CryptoStreamMode.Write);

				if(_compression)
					// Creates a GZipStream to compress the file.
					outStream = new GZipStream(outStream, CompressionMode.Compress);

				// Saves the serialized object in a buffer.
				byte[] buffer = _formatter.Serialize(objectToSave);

				// Writes the buffer
				outStream.Write(buffer, 0, buffer.Length);
			}
			catch(Exception e) { PrintError(e); }
			finally
			{
				// Closes the stream if not already closed.
				if(outStream != null)
					outStream.Dispose();
			}
		}
		#endregion


		#region Public Methods
		/// <summary>
		/// Sets a method that determinates the name of the file for the given object.
		/// </summary>
		public Func<T, string> SetFileName;


		/// <summary>
		/// Blocks the calling thread until all the launched tasks have finished.
		/// </summary>
		public void JoinCurrentTasks() { Task.WaitAll(_pendingTasks); }


		/// <summary>
		/// Loads the T objects from the specified path.
		/// </summary>
		/// <param name="onObjectReady">Delegate called each time a T object is ready.</param>
		/// <returns>The number of objects loaded.</returns>
		public Task<int> LoadObjectAsync(Action<T> onObjectReady)
		{
			return Task.Factory.StartNew(delegate()
			{
				/**
				 * Search for files of the specified
				 * extension in the given directory.
				 */
				IEnumerable<string> userFiles = Directory.EnumerateFiles(_dirPath, "*." + _fileExtension, SearchOption.TopDirectoryOnly);

				int filesCount = userFiles.Count();

				// If there's at least one file
				if(filesCount > 0)
				{
					// Waits all the tasks launched by the previous operation.
					JoinCurrentTasks();
					_pendingTasks = new Task[filesCount];

					/**
					 * Reads, decompresses and an builds an object in 
					 * a different task then calls onObjectReady.
					 */
					for(int i = 0; i < filesCount; i++)
						_pendingTasks[i] = Task.Factory.StartNew(LoadObjectTask, new Tuple<string, Action<T>>(userFiles.ElementAt(i), onObjectReady));

					JoinCurrentTasks();
				}

				return filesCount;
			}, TaskCreationOptions.LongRunning);
		}

		/// <summary>
		/// Saves the given objects in files
		/// </summary>
		/// <param name="objects">Array containing the objects to save.</param>
		public Task SaveObjectsAsync(IEnumerable<T> objects)
		{
			if(objects.Any())
				return Task.Factory.StartNew(delegate()
				{
					// Waits all the tasks launched by the previous operation
					JoinCurrentTasks();
					_pendingTasks = new Task[objects.Count()];

					// Each object is saved indipendently.
					for(int i = 0; i < objects.Count(); i++)
						_pendingTasks[i] = Task.Factory.StartNew(SaveFileTask, objects.ElementAt(i));

					JoinCurrentTasks();
				});
			else
				return Task.CompletedTask;
		}
		#endregion
	}
}
