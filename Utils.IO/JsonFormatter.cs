using System.IO;
using System.Text;
using System.Web.Script.Serialization;

namespace Mek_lib.Utils.IO
{
	/// <summary>
	/// Transforms a <typeparamref name="T"/> object in json and back.
	/// </summary>
	public class JsonFormatter<T> : Formatter<T>
	{
		protected readonly JavaScriptSerializer _serializer;

		public JsonFormatter()
		{
			_serializer = new JavaScriptSerializer();
		}

		/// <summary>
		/// Gets the default file extension.
		/// </summary>
		/// <value>The default file extension.</value>
		public override string DefaultFileExtension { get { return "json"; } }


		/// <summary>
		/// Transforms the given <typeparamref name="T"/> object to a Json string in a byte array.
		/// </summary>
		/// <param name="objectToSerialize">The <typeparamref name="T"/> object to serialize.</param>
		/// <returns>The object in a serialized, byte form.</returns>
		public override byte[] Serialize(T objectToSerialize)
		{
			// Serializes the object into a Json string and converts it to a byte array
			return Encoding.Default.GetBytes(SerializeToString(objectToSerialize));
		}


		/// <summary>
		/// Transforms the given <typeparamref name="T"/> object to a Json formatted string.
		/// </summary>
		/// <param name="objectToSerialize">The T object to serialize.</param>
		/// <returns>The object in a serialized, string form.</returns>
		public string SerializeToString(T objectToSerialize)
		{
			// Serializes the object into a Json string
			return _serializer.Serialize(objectToSerialize);
		}


		/// <summary>
		/// Transforms the given stream contenent from Json formatted text to a <typeparamref name="T"/> object.
		/// </summary>
		/// <param name="inStream">The stream get the Json text from.</param>
		/// <returns>The object built from the stream.</returns>
		public override T Deserialize(Stream inStream)
		{
			TextReader tr = new StreamReader(inStream);

			return _serializer.Deserialize<T>(tr.ReadToEnd());
		}
	}
}
