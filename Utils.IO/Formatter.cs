using System.IO;

namespace Mek_lib.Utils.IO
{
	/// <summary>
	/// Base class for implementing serialization and deserialization classes to a <typeparamref name="T"/> object to a given format.
	/// </summary>
	public abstract class Formatter<T>
	{
		/// <summary>
		/// Gets the default extension for the file of the given format.
		/// </summary>
		/// <returns>The default file extension.</returns>
		public abstract string DefaultFileExtension { get; }


		/// <summary>
		/// Transforms the given T object to the implemented format
		/// </summary>
		/// <param name="objectToSerialize">The T object to serialize</param>
		/// <returns>The object in a serialized, byte array</returns>
		public abstract byte[] Serialize(T objectToSerialize);


		/// <summary>
		/// Transforms the given stream contenent from a text form in the implemented format to a T object
		/// </summary>
		/// <param name="inStream">The stream get the formatted text from</param>
		/// <returns>The object built from the stream</returns>
		public abstract T Deserialize(Stream inStream);
	}
}